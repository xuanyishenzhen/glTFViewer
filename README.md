# glTFViewer

#### 介绍
用Vulkan实现的glTF模型查看器，主要用学习glTF文件格式。

支持glTF模型的PBR渲染，支持骨骼动画。

截图如下：
  ![截图](./data/screenshot.png)

#### 安装教程

1.  依赖
    * [Vulkan SDK](https://vulkan.lunarg.com/)    Vulkan是OpenGL的下一代版本，和DirectX 12一样都是基于AMD私有的Mantle API，不同的是Vulkan是开源的图形API。
    * [draco](https://github.com/google/draco)     draco是一个用于压缩和解压缩三维几何体网格和点云的库。其目的是改进三维图形的存储和传输。
    * [glfw](https://github.com/glfw/glfw)      用于OpenGL、OpenGL ES和Vulkan的跨平台窗口和输入抽象库，支持Windows/MacOS和Linux。
    * [imgui](https://github.com/ocornut/imgui) : Dear ImGui: Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies.
    * [imguizmo](https://github.com/CedricGuillemet/ImGuizmo) : Immediate mode 3D gizmo for scene editing and other controls based on Dear Imgui.
    * [spdlog](https://github.com/gabime/spdlog) : Fast C++ logging library.
    * [stb](https://github.com/nothings/stb) : Single-file public domain (or MIT licensed) libraries for C/C++.
    * [tinygltf](https://github.com/syoyo/tinygltf) : Header only C++11 tiny glTF 2.0 library


#### 编译说明

1.  MacOS平台
    * 下载并安装[Vulkan SDK](https://vulkan.lunarg.com/)
    * 下载glTFViewer代码
        ```
        git clone --recursive https://gitee.com/xuanyishenzhen/glTFViewer.git
        ```
        或者
        ```
        git clone https://gitee.com/xuanyishenzhen/glTFViewer.git
        git submodule init
        git submodule update
        ```
    * 生成Makefile文件
        ```
        cd glTFViewer
        mkdir build
        cd build
        cmake ..
        ```
    * 编译
        ```
        make
        ```
    * 运行
        ```
        ./glTFViewer
        ```
        注意：必须在build目录中运行glTFViewer，因为程序中需要加载的资源(如shader/model等)都是通过相对路径获取。

2.  Window平台
    * 下载并安装[Vulkan SDK](https://vulkan.lunarg.com/)
    * 下载glTFViewer代码
        ```
        git clone --recursive https://gitee.com/xuanyishenzhen/glTFViewer.git
        ```
    * 生成Visual Studio 2019的工程文件
        ```
        cd glTFViewer
        mkdir build
        cd build
        cmake -G "Visual Studio 16 2019" ..
        ```
    * 编译
        ```
        msbuild glTFViewer.sln
        ```
    * 运行
        ```
        ./Debug/glTFViewer.exe
        ```
        注意：必须在build目录中运行glTFViewer，因为程序中需要加载的资源(如shader/model等)都是通过相对路径获取。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 感谢

本项目代码源自SaschaWillems的[Vulkan-glTF-PBR](https://github.com/SaschaWillems/Vulkan-glTF-PBR)项目。
